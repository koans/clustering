# Clustering

## Problem

Given a list of points and a distance threshold,
classify these points into clusters based on the distance threshold.

## (Naive) Algorithm

Iterate over all points, and put each point,
and its "neighbors" (and their neighbors, recursively), into a new cluster.
If a point already belongs to a cluster, then skip.
At the end of the loop,
all points should have been classified into one and only one cluster.

## How to Build and Run this Test App

```
cd clustering
./gradlew build
./gradlew run
```

The repo includes essentially one source file:
https://gitlab.com/koans/clustering/blob/master/clustering/src/main/kotlin/clustering/App.kt
The "main" function includes some simple test cases.
