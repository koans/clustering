/*
 * Given a list of points and a distance threshold,
 *     classify these points into clusters based on the distance threshold.
 */
package clustering

import kotlin.math.pow

// 2D point.
data class Point(val x: Double, val y: Double)

fun distance(p1: Point, p2: Point): Double {
    return Math.sqrt((p1.x - p2.x).pow(2.0) + (p1.y - p2.y).pow(2.0))
}

// Given a list of 2d points, and a distance threshold,
//     it creates clusters of points.
// Returns a map of {cluster id -> cluster (a set of points) }
fun findClusters(points: List<Point>, dist: Double): Map<Int, Set<Point>> {
    require(dist >= 0) {
        "Distance threshold must be non-negative, was $dist"
    }

    // TBD:
    // All input points should be distinct??

    // No points, no clusters.
    if(points.isEmpty()) {
        return emptyMap<Int, Set<Point>>()
    }

    // One point, one cluster.
    val size = points.size
    if(size == 1) {
        return mapOf(0 to HashSet(points))
    }
   
    // Note that we consider dist==0 a valid input.
    // TBD: This does not work if there are multiple points in the same location.
    if(dist == 0.0) {
        return points.mapIndexed { idx, it -> idx to setOf(it) }.toMap()
    }

    // Build pair distance matrix
    // In our use case, always i < j. Hence, we only compute the half.
    val pairs = Array(size) {DoubleArray(size) {0.0}}
    for(i in 0..size-1) {
        for(j in i+1..size-1) {
            pairs[i][j] = distance(points[i], points[j])
        }
    }

    // Build a cluster from the given point p1.
    // We assume that p1 belongs to the cluster.
    fun findNearbyPoints(p1: Point, cluster: MutableSet<Point>): Unit {
        for(j in 0..size-1) {
            val p2 = points[j]
            if(p1 != p2 && p2 !in cluster) {
                val d = distance(p1, p2)
                if(d <= dist) {
                    cluster.add(p2)
                    findNearbyPoints(p2, cluster)
                }
            }
        }
    }

    // Cluster id, just a sequentially incremented number.
    var index = 0
    // The "result", initialized as an empty map.
    // Note: We could have just used List of Set (without "cluster id").
    val map = mutableMapOf<Int, Set<Point>>()

    // Checks if the given point is already in any of the "found" clusters.
    // Obviously, this is very inefficient.
    // A better way is to maintain a reverse map, something like {point -> cluser id},
    //    while adding points into clusters, and use that map for this purpose.
    fun isInAnyCluster(p: Point): Boolean {
        for((_,v) in map) {
            if(p in v) {
                return true
            }
        }
        return false;
    }

    // "Main" loop.
    for(i in 0..size-1) {
        val p1 = points[i]

        // if p1 has already been classified into any of the clusters, skip.
        if(isInAnyCluster(p1)) {
            continue;
        }

        val cluster = mutableSetOf(p1)
        findNearbyPoints(p1, cluster)
        map[index++] = cluster
    }

    return map
}

// Quick test cases.
fun main(args: Array<String>) {
    val points1 = listOf(Point(1.0,1.0))
    val points2 = listOf(Point(1.0,1.0), Point(1.0,1.5), Point(1.5,1.0))
    val points3 = listOf(Point(1.0,1.0), Point(1.0,1.5), Point(1.5,1.0), 
                        Point(1.0,5.0), Point(1.0,5.5), Point(1.5,5.0),
                        Point(5.0,1.0), Point(5.0,1.5), Point(5.5,1.0))
    val points4 = listOf(Point(1.0,1.0), Point(5.0,1.0), Point(1.0,5.0))
    val dist = 2.0
    var clusters = findClusters(points1, dist)
    println(clusters)
    clusters = findClusters(points2, dist)
    println(clusters)
    clusters = findClusters(points3, dist)
    println(clusters)
    clusters = findClusters(points4, dist)
    println(clusters)
    clusters = findClusters(points4, dist = 0.0)
    println(clusters)
}
